import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegistrationsHomeComponent } from './components/registrations-home/registrations-home.component';
import { RegistrationSemesterComponent } from './components/registrations-semester/registrations-semester.component';
import { RegistrationCoursesComponent } from './components/registrations-courses/registrations-courses.component';
import { RegistrationListComponent } from './components/registrations-list/registrations-list.component';
import { AuthGuard } from '../auth/guards/auth.guard';
import {RegistrationCheckoutComponent} from "./components/registration-checkout/registration-checkout.component";

const routes: Routes = [
    {
        path: '',
        component: RegistrationsHomeComponent,
        canActivate: [
            AuthGuard
        ],
        children: [
            {
                path: '',
                redirectTo: 'semester',
            },
            {
                path: 'semester',
                component: RegistrationSemesterComponent
            },
            {
                path: 'courses',
                children: [
                    {
                        path: '',
                        redirectTo: 'overview',
                    },
                    {
                        path: 'overview',
                        component: RegistrationCoursesComponent
                    },
                    {
                        path: 'checkout',
                        component: RegistrationCheckoutComponent
                    }
                ]
            },
            {
                path: 'list',
                component: RegistrationListComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationsRoutingModule {
}
