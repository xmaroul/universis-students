import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileHomeComponent } from './components/profile-home/profile-home.component';
import {TranslateModule, TranslateService} from "@ngx-translate/core";
import {MostModule} from "@themost/angular";
import {ProfileRoutingModule} from "./profile-routing.routing";
import { ProfileDetailsComponent } from './components/profile-details/profile-details.component';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';
import {environment} from "../../environments/environment";
import {ProfileSharedModule} from "./profile-shared.module";
import { ProfileService } from './services/profile.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
      SharedModule,
    MostModule,
    ProfileSharedModule,
    ProfileRoutingModule,
    SharedModule
  ],
  declarations: [
      ProfileHomeComponent, 
      ProfileDetailsComponent,
      ProfilePreviewComponent
  ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [

  ]
})
export class ProfileModule {

    constructor() {
        //
    }
}
