import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-requests-new',
  templateUrl: './requests-new.component.html',
  styleUrls: ['./requests-new.component.scss']
})
export class RequestsNewComponent implements OnInit {

  constructor(private _context: AngularDataContext) { }

  ngOnInit() {
  }
  sendSimpleMessage(title: string, description: string) {
    return this._context.model('RequestMessageActions')
      .save({
        name: title,
        description: description
      })
      .then(() => {
        window.location.href = ('/#/requests/list');
      }).catch((err) => {
        console.log(Error);
      });
  }}
