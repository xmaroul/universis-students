import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress-bar-semester',
  templateUrl: './progress-bar-semester.component.html',
  styleUrls: ['./progress-bar-semester.component.scss']
})
export class ProgressBarSemesterComponent implements OnInit {

  public isactive = false;
  public ishide = true;

  public isLoading = true;   // start to load data

  constructor() { }

  ngOnInit() {

    this.isLoading = false; // end to load data
  }

}
