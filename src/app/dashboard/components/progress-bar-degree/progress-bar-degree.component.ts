import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../../../profile/services/profile.service';
import {GradesService} from '../../../grades/services/grades.service';
import {GradeScale} from '../../../grades/services/grade-scale.service';

let defaultGradeScale: GradeScale;

@Component({
  selector: 'app-progress-bar-degree',
  templateUrl: './progress-bar-degree.component.html',
  styleUrls: ['./progress-bar-degree.component.scss']
})
export class ProgressBarDegreeComponent implements OnInit {

  public currentIndex: number;
  public toShowSmallDots = true;
  public isLoading = true;   // Only if data is loaded

  public SemesterPoints: any[] = [
    {number: 1, active: false, complete: false, passed: 0, averageGrades: 0.0, indicator: ''},
    {number: 2, active: false, complete: false, passed: 0, averageGrades: 0.0, indicator: ''},
    {number: 3, active: false, complete: false, passed: 0, averageGrades: 0.0, indicator: ''},
    {number: 4, active: false, complete: false, passed: 0, averageGrades: 0.0, indicator: ''},
    {number: 5, active: false, complete: false, passed: 0, averageGrades: 0.0, indicator: ''},
  ];

  constructor(private profileService: ProfileService,
              private gradesService: GradesService) { }

  ngOnInit() {
    this.profileService.getStudent().then(res => {

      const student = res; // Load data

      // check if current semester is greater than max semesters of studyProgram
      if ( student.studyProgram.semesters < 4 ) {
        this.toShowSmallDots = false;
      }

      this.gradesService.getAllGrades().then(( grades ) => {
        const allGrades = grades.value;

        const passedCourses = allGrades.filter((x) => x.isPassed);
        const passedCourseCount = passedCourses.length;

        // set average of passed courses
        const passedGradeAverage0 = passedCourseCount ? passedCourses.reduce((a, b) => a + b.grade1, 0) / passedCourseCount : 0;
        if (!defaultGradeScale) {

          // get default grade scale
          this.gradesService.getDefaultGradeScale().then(gradeScale => {
            defaultGradeScale = gradeScale;
            gradeScale.formatPrecision = 1 ;
            const passedGradeAverage = gradeScale.format(passedGradeAverage0);
            this.fillData(student, passedCourseCount, passedGradeAverage);
            this.isLoading = false; // Data is loaded
          });
        } else {
          const passedGradeAverage = defaultGradeScale.format(passedGradeAverage0);
          this.fillData(student, passedCourseCount, passedGradeAverage);
          this.isLoading = false; // Data is loaded
        }
      });
    });
  }

  fillData(student: any, passedCourseCount, passedGradeAverage) {

    if (student.semester > student.studyProgram.semesters) { // if the student is in a longer semester than the studyProgram

      const n = student.semester;

      // set numbers and states for others points
      for (let i = 0; i < 4; i++) {
        this.SemesterPoints[i].complete = true; // previus semesters
        this.SemesterPoints[i].number = (n + i) - 4;
        this.SemesterPoints[i].indicator = this.calcIndicator(this.SemesterPoints[i].number);
      }

      this.currentIndex = 4;
      // current point
      this.SemesterPoints[4].number = student.semester;
      this.SemesterPoints[4].active = true;
      this.SemesterPoints[4].passed = passedCourseCount;
      this.SemesterPoints[4].averageGrades = passedGradeAverage;
      if (student.studentStatus.alternateName === 'graduated') {
        this.SemesterPoints[4].complete = true;
      }

    } else if (student.semester >= 1 && student.semester <= 4) { // if the student is from 1st to 4th semester
      const n = student.semester;

      // set state for others points
      for (let i = 0; i < 4; i++) {
        if (i < n - 1) {
          this.SemesterPoints[i].complete = true;
        }
        this.SemesterPoints[i].indicator = this.calcIndicator(this.SemesterPoints[i].number);
      }

      this.currentIndex = n - 1;
      // current semester
      this.SemesterPoints[n - 1].active = true;
      this.SemesterPoints[n - 1].number = n;
      this.SemesterPoints[n - 1].passed = passedCourseCount;
      this.SemesterPoints[n - 1].averageGrades = passedGradeAverage;
      this.SemesterPoints[n - 1].indicator = this.calcIndicator(this.SemesterPoints[n - 1].number);

    } else if (student.semester > 4) { // if the student is more than 4 months old

      const n = student.semester;

      // set numbers and states for others points
      for (let i = 0; i < 3; i++) {
        this.SemesterPoints[i].complete = true; // previus semesters
        this.SemesterPoints[i].number = (n + i) - 3;
        this.SemesterPoints[i].indicator = this.calcIndicator(this.SemesterPoints[i].number);
      }

      this.currentIndex = 3;
      // current semester
      this.SemesterPoints[3].number = student.semester;
      this.SemesterPoints[3].active = true;
      this.SemesterPoints[3].passed = passedCourseCount;
      this.SemesterPoints[3].averageGrades = passedGradeAverage;
      this.SemesterPoints[3].indicator = this.calcIndicator(this.SemesterPoints[3].number);
    } else if (student.semester === 0) {
      for (let i = 0; i < 4; i++) {
        this.SemesterPoints[i].indicator = this.calcIndicator(this.SemesterPoints[i].number);
      }
    }
  }

  calcIndicator(number) {
    if (number === 1) {
      return 'Indicator1st';
    } else  if (number === 2) {
      return 'Indicator2nd';
    } else  if (number === 3) {
      return 'Indicator3nd';
    } else {
      return 'IndicatorGreater4Th';
    }
  }
}
